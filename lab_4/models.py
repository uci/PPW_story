from django.db import models
from django.utils import timezone
from datetime import date, datetime

class Schedule(models.Model):
	hour_min = models.TimeField(auto_now = False, auto_now_add = False)
	activity = models.CharField(max_length=27)
	place = models.CharField(max_length=27)
	category = models.CharField(max_length=27)

