from django import forms
import datetime
from .models import Schedule

class ScheduleForm(forms.Form):
	hour_min = forms.TimeField(label="Hour : Min", widget = forms.TimeInput(attrs={'type' : 'time'}))
	activity = forms.CharField(label="Activity")
	place = forms.CharField(label="Location")
	category = forms.CharField(label="Category")


	