from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views
app_name = 'lab_4'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.mainHome, name="mainHome"),
    path('profil/', views.profile, name="myProfile"),
    path('register/', views.register, name="registerHere"),
    path('add_schedule/', views.schedule_form, name="addSchedule"),
    path('schedule_list/', views.schedule_list, name="scheduleList"),
    path('display/', views.display_form, name="display"), 
    path('schedule_remove/', views.remove_schedule, name="schedule_remove")
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
